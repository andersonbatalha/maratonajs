# Maratona JS

## Tópicos principais

1. Requisitos básicos

* npm
* node
* MySQL

2. Iniciando o projeto
* Crie uma nova pasta e dentro dela rode o comando:
    ```
    npm init
    ```

* Preencha as informações solicitadas

3. Instalar a dependência
    ```
    $ npm install express
    ```

1. NodeJS / Express
    1. Configurando rotas de autenticação e o nodemon

    * Criar arquivo auth.js dentro de uma nova pasta 'controllers'
    * Dentro do arquivo auth.js, criar as rotas:
        ```
        const express = require('express');

        const routes = express.Router();

        routes.get("/sign-in", (req, res) => {
            res.json("Sign in");
        });

        routes.get("/sign-up", (req, res) => {
            res.json("Sign up");
        });

        module.exports = routes;
        ```
    * No arquivo index.js, fazer a referência ao controller

        ```
        const authController = require("../controllers/auth");

        app.use("/auth", authController);
        ```

    * Instalar a dependência nodemon
        * Executar no terminal   
            ```
            $ npm install --save nodemon
            ```
        * Colocar no arquivo package.json
            ```
            "scripts": {
                "start": "nodemon src/index.js"
            },
            ```

        * Rodar o projeto com `npm start`
        * Assim, não é necessário mais executar novamente o projeto a cada mudança

    2. Configurando o dotenv e o sequelize

        * Sequelize
            * Instalação

                `npm install --save sequelize@5.2.10`
            
            * Criar os models
                
                `npx sequelize-cli init:models`

            * Criar o arquivo de configuração do banco de dados
                `npx sequelize-cli init:config`

        * Dotenv
            * Instalação
                
                `npm install --save dotenv`

            * Criar o arquivo .env
                ```
                DB_USER=root
                DB_PASSWORD=12345
                DB_HOST=127.0.0.1
                DB_NAME=links
                ```
            
            * Alterar o arquivo config.json para config.js

                ```
                require('dotenv').config();

                module.exports = {
                    "development": {
                        "username": process.env.DB_USER,
                        "password": process.env.DB_PASSWORD,
                        "database": process.env.DB_NAME,
                        "host": process.env.DB_HOST,
                        "dialect": "mysql"
                    },
                    "test": {
                        "username": "root",
                        "password": null,
                        "database": "database_test",
                        "host": "127.0.0.1",
                        "dialect": "mysql"
                    },
                    "production": {
                        "username": "root",
                        "password": null,
                        "database": "database_production",
                        "host": "127.0.0.1",
                        "dialect": "mysql"
                    }
                }
                ```

        * Instalar driver do sequelize para o MySQL

            `npm install --save mysql2`

        * Alterar a variável `config` em models/index.js
            ```
            const config = require(__dirname + '/../config/config.json')[env];


            const config = require(__dirname + '/../config/config.js')[env];
            ```
        * Alterar arquivo em src/index.js

            ```
            db.sequelize.sync().then(()=>{
                app.get("/", (req, res) => {
                    res.json("API running");
                });
            });
            ```

    3. Configurando os models do sequelize e criptografando senhas
        * Criando models
            * Crie o model 'Account' dentro da pasta models
                ```
                module.exports = (sequelize, DataTypes) => {
                    const Account = sequelize.define('Account', {
                        email: {
                            type: DataTypes.STRING,
                            allowNull: false,
                            defaultValue: "email@email.com"
                        },
                        password: {
                            type: DataTypes.STRING,
                            allowNull: false,            
                        }
                    });
                    return Account;
                };
                ```

            * No arquivo controllers/auth.js, modifique a rota para inserir um registro no banco de dados

                ```
                routes.get("/sign-up", async (req, res) => {
                    const result = await Account.create({email: "andersonpbatalha@gmail.com", password: "andersonapb123"});
                    res.json("Sign up");
                });
                ```

        * Criptografando senhas

            * Instale o pacote bcrypt

                `npm install --save bcrypt`
            
            * Altere a rota para criptografar a senha criada
                ```
                routes.get("/sign-up", async (req, res) => {
                    const email = "andersonpbatalha@gmail.com";
                    const password = "andersonapb123";
                    
                    const hash = bcrypt.hashSync(password, 10);

                    const result = await Account.create({email, password: hash});
                    res.json(result);
                });
                ```
            
            * Altere o prototype do objeto 

                ```
                Account.prototype.toJSON = function() {
                    const values = { ...this.get() };
                    delete values.password;

                    return values;
                }            
                ``` 
    4. Padronizando respostas das requisições
    5. Validação de dados com mensagens traduzidas
    6. Implementando JSON Web Tokens
    7. CRUD de itens do banco
    8. Criando um middleware de autenticação
    9. Removendo rotas da verificação do JWT
    10. Refresh Token parte 1 / MySQL migrations
        * Criar uma migração
            
            `npx sequelize-cli migration:create --name migration_name`

        * Realizar migrações
            
            `npx sequelize-cli db:migrate`

    11. Adicionando CORS no Express
        1. `npm install --save cors`
        2. Adicionar `const cors = require('cors')` no arquivo src/index.js

2. React

    1. Criando uma aplicação React com Create React App

        * Instalação
            
            `npx create-react-app nome_app`

        * No arquivo index.js
            
            ```
            import React from 'react';
            import ReactDOM from 'react-dom';
            
            ReactDOM.render(null, document.getElementById('root'));
            ```            
    
    2. Criando rotas no React
    3. Adicionando Bootstrap
        * Instalação
            `npm install --save bootstrap`
    
    4. Adicionando SASS

        * Instalação
            `npm install --save-dev node-sass`

    5. Implementando o UI Kit
    6. Criando as telas do projeto
    7. Configurando redux
        
        * Instalação
            `npm install --save redux redux-promise react-redux`

    8. Usando axios para conectar com a API
    9. Salvando JWT no front-end
        * Instalar o pacote universal-cookie
            `npm install --save universal-cookie`

    10. Implementando o login de usuário
    11. Refatorando e reorganizando actions e reducers
    12. Inserindo itens na API
    13. Listando itens da API
    14. Mantendo o usuário logado
    15. Listando item da API e gerenciando inputs
    16. Editando itens da API
    17. Deletando itens da API
    18. Verificando o refresh token
    19. 
    20. 
    21. 
    22. 
    23. 
    24. 
    25. 