/** Arquivo principal do node.js */

const express = require('express');
const cors = require('cors');
const app = express();
const db = require('../src/models'); // importa as configurações do sequelize na pasta models
const authController = require("../src/controllers/auth"); // importa o controller responsável pela autenticação
const linkController = require("../src/controllers/link"); // importa o controller responsável pelo CRUD dos links
const response = require('../src/middlewares/response');
const checkJwt = require('../src/middlewares/jwt');

app.use(cors()); // padronizar a resposta para requisições
app.use(response); // padronizar a resposta para requisições
app.use(checkJwt);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/auth", authController);
app.use("/link", linkController);

app.listen(3001, () => { // configura o servidor para escutar na porta 3001 
    console.log("Listening on port 3001");
});

db.sequelize.sync().then(()=>{ // sincroniza as alterações no banco de dados ao executar o projeto
    app.get("/", (req, res) => { 
        res.json("API running");
    });
});


