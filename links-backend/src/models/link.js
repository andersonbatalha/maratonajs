/** Cria a estrutura da tabela Link */

module.exports = (sequelize, DataTypes) => {
    const Link = sequelize.define('Link', { // define a tabela Link com dois campos 
        label: {
            type: DataTypes.STRING, // tipo de dados
            allowNull: false, // NOT NULL
        },
        url: {
            type: DataTypes.STRING, // tipo de dados
            allowNull: false, // NOT NULL
        },
        image: {
            type: DataTypes.STRING, // tipo de dados
            allowNull: false, // NOT NULL
            defaultValue: 'image.jpg',
        },
        isSocial: {
            type: DataTypes.BOOLEAN, // tipo de dados
            allowNull: false, // NOT NULL
            defaultValue: 0,
        },

    });

    Link.associate = (models) => {
        Link.belongsTo(models.Account, { foreignKey: 'accountId' }); 
    };

    return Link;
};