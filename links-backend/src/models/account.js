/** Cria a estrutura da tabela Account */

module.exports = (sequelize, DataTypes) => {
    const Account = sequelize.define('Account', { // define a tabela Account com dois campos (email e password)
        email: {
            type: DataTypes.STRING, // tipo de dados
            allowNull: false, // NOT NULL
            defaultValue: "email@email.com" // valor default
        },
        password: {
            type: DataTypes.STRING, // tipo de dados
            allowNull: false, // NOT NULL
        },
        jwtVersion: {
            type: DataTypes.INTEGER, // tipo de dados
            allowNull: false, // NOT NULL
            defaultValue: 0, 
        }
    });

    Account.associate = (models) => {
        Account.hasMany(models.Link, { foreignKey: 'accountId' }); 
    };

    Account.prototype.toJSON = function() {
        const values = { ...this.get() };
        delete values.password;

        return values;
    }

    return Account;
};