'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return queryInterface.addColumn("Accounts", "jwtVersion", {
      type: Sequelize.INTEGER, // tipo de dados
      allowNull: false, // NOT NULL
      after: "password",
      defaultValue: 0,
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return queryInterface.removeColumn("Accounts", "jwtVersion");
    }
};
