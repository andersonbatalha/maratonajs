'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return queryInterface.changeColumn('Links', 'image', {
      type: Sequelize.STRING, // tipo de dados
      allowNull: false, // NOT NULL
      defaultValue: "image.jpg",
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return queryInterface.changeColumn('Links', 'image', {
      type: Sequelize.STRING, // tipo de dados
      allowNull: false, // NOT NULL
    });
  }
};
