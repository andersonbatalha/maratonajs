const express = require('express');
const bcrypt = require('bcrypt');
const routes = express.Router();
const { Account } = require('../models');
const { accountSignUp, accountSignIn } = require('../validators/account');
const { getMessages } = require('../helpers/messages');
const { generateJwt, generateRefreshJwt, getTokenFromHeaders, verifyRefreshJwt } = require('../helpers/jwt');

const saltRounds = 10;

/** Criadas rotas para o app */
routes.post("/sign-in", accountSignIn, async (req, res) => {
    const { email, password } = req.body;

    /** Consulta que verifica se há um email já cadastrado */
    const account = await Account.findOne({ where: { email } });

    if (!account) return res.jsonBadRequest(null, getMessages("account.signin.email.email_not_exists")); // Se e-mail não existe, retorna mensagem de erro

    const match = account ? bcrypt.compareSync(password, account.password) : null;
    if (!match) return res.jsonBadRequest(null, getMessages("account.signin.passwords_not_match"));
    
    const token = generateJwt({ id: account.id });
    const refreshToken = generateRefreshJwt({ id: account.id, version: account.jwtVersion });

    return res.jsonOK(account, getMessages("account.signin.success"), { token, refreshToken }); // se o login foi efetuado, retorna mensagem de sucesso

    res.jsonOK();
});

routes.post("/sign-up", accountSignUp, async (req, res) => {
    const { email, password } = req.body;

    /** Consulta que verifica se há um email já cadastrado */
    const account = await Account.findOne({ where: { email } });
    
    if (account) {
        return res.jsonBadRequest(null, getMessages("account.signup.password.email_exists")); // Se e-mail já existe, retorna mensagem de erro
    }
    
    const hash = bcrypt.hashSync(password, saltRounds); // criptografa a senha do usuário
    const newAccount = await Account.create({email, password: hash}); // insere um novo registro no banco de dados
    
    const token = generateJwt( {id: newAccount.id} );
    const refreshToken = generateRefreshJwt({id: newAccount.id, version: newAccount.jwtVersion});

    return res.jsonOK(newAccount, getMessages("account.signup.success"), { token, refreshToken }); // se o objeto foi criado, retorna na tela
});

routes.post("/refresh", async (req, res) => {
    let token = getTokenFromHeaders(req.headers);
    if (!token) return res.jsonUnauthorized(null, "Invalid token.");


    try {
        const decoded = verifyRefreshJwt(token);
        const account = await Account.findByPk(decoded.id);
        
        if ((!account) || (decoded.version !== account.jwtVersion)) return res.jsonUnauthorized(null, "Invalid token.");

        const meta = { // envia como os metadados da requisição
            token: generateJwt({ id: account.id }),
        }

        return res.jsonOK(null, null, meta);
    } catch (error) {
        return res.jsonUnauthorized(null, "Invalid token.");
    }
});


module.exports = routes;