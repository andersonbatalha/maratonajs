const express = require('express');
const routes = express.Router();
const { Link, Account } = require('../models');

routes.get("/", async (req, res) => {
    const { accountId } = req;
    const links = await Link.findAll({ where: { accountId } });
    return res.jsonOK(links, `Links (id = ${accountId})`);
});

routes.get("/:id", async (req, res) => {
    const { accountId } = req;
    const { id } = req.params;
    const link = await Link.findOne({ where: { id, accountId } });

    if (!link) return res.jsonNotFound(null, `Não encontrado! (id=${id})`);

    return res.jsonOK(link, `<Link>: ${ link.label }`);
});

routes.post("/", async (req, res) => {
    const { accountId, body } = req;
    const { label, url, image, isSocial } = body;
    const link = await Link.create({
        label, url, image, isSocial, accountId
    });

    return res.jsonOK(link);
});

routes.put("/:id", async (req, res) => {
    const { accountId, body } = req;
    const id = req.params;
    const fields = ['label', 'url', 'isSocial'];
    const link = await Link.findOne({ where: id,  accountId });

    if (!link) return res.jsonNotFound(null, "Não encontrado");

    fields.map(fieldName => {
        const newValue = body[fieldName];
        if (newValue !== undefined) link[fieldName] = newValue;        
    });
    await link.save();

    return res.jsonOK(link, "Link alterado com sucesso!");
});

routes.delete("/:id", async (req, res) => {
    const { accountId } = req;
    const { id } = req.params;

    const link = await Link.findOne({ where: { id, accountId } });

    await link.destroy();

    return res.jsonOK(null, `Removido com sucesso o registro ${id}`);
});


module.exports = routes;
