const TYPE_CONTENT = 'application/json';
const STATUS_CODE_OK = 200;
const STATUS_CODE_BAD_REQUEST = 400;
const STATUS_CODE_UNAUTHORIZED = 401;
const STATUS_CODE_NOT_FOUND = 404;
const STATUS_CODE_SERVER_ERROR = 500;

const { getMessages } = require('../helpers/messages');

/** A function jsonOK padroniza a resposta as requisições */
const jsonOK = function(data, message, metadata) { 
    const status = STATUS_CODE_OK;
    message = (message) ? message : getMessages("response.json_ok"); 
    metadata = (metadata) ? metadata : {};

    this.status(STATUS_CODE_OK);// código 200: sucesso
    this.type(TYPE_CONTENT); // tipo de dados que será passado

    return this.json({ message, data, metadata, status: status });
};

const jsonBadRequest = function(data, message, metadata) { 
    const status = STATUS_CODE_BAD_REQUEST;
    message = (message) ? message : getMessages("response.json_bad_request"); 
    metadata = (metadata) ? metadata : {};

    this.status(STATUS_CODE_BAD_REQUEST);// código 400: bad request
    this.type(TYPE_CONTENT); // tipo de dados que será passado

    return this.json({ message, data, metadata, status: status });
};

const jsonUnauthorized = function(data, message, metadata) { 
    const status = STATUS_CODE_UNAUTHORIZED;
    message = (message) ? message : getMessages("response.json_unauthorized"); 
    metadata = (metadata) ? metadata : {};

    this.status(STATUS_CODE_UNAUTHORIZED);// código 401: unauthorized
    this.type(TYPE_CONTENT); // tipo de dados que será passado

    return this.json({ message, data, metadata, status: status });
};

const jsonNotFound = function(data, message, metadata) { 
    const status = STATUS_CODE_NOT_FOUND;
    message = (message) ? message : getMessages("response.json_not_found"); 
    metadata = (metadata) ? metadata : {};

    this.status(STATUS_CODE_NOT_FOUND);// código 404: not found
    this.type(TYPE_CONTENT); // tipo de dados que será passado

    return this.json({ message, data, metadata, status: status });
};

const jsonServerError = function(data, message, metadata) { 
    const status = STATUS_CODE_SERVER_ERROR;
    message = (message) ? message : getMessages("response.json_server_eror"); 
    metadata = (metadata) ? metadata : {};

    this.status(STATUS_CODE_SERVER_ERROR);// código 500: server error
    this.type(TYPE_CONTENT); // tipo de dados que será passado

    return this.json({ message, data, metadata, status: status });
};

const response = (req, res, next) => {
    res.jsonOK = jsonOK;
    res.jsonBadRequest = jsonBadRequest;
    res.jsonNotFound = jsonNotFound;
    res.jsonUnauthorized = jsonUnauthorized;
    res.jsonServerError = jsonServerError;

    next();
};

module.exports = response;