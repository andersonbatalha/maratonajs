const { verifyJwt } = require('../helpers/jwt');

const checkJwt = (req, res, next) => {
    let { url: path } = req;
    const excludedPaths = ['/auth/sign-up', '/auth/sign-in', '/auth/refresh'];
    const isExcluded = !!excludedPaths.find(p => p.startsWith(path));

    console.log(path, isExcluded);

    if (isExcluded) return next();
 
    let token = req.headers['authorization'];
    token = token ? token.slice(7, token.length) : null;
    if (!token) return res.jsonUnauthorized(null, "Invalid token");

    try {   
        const decoded = verifyJwt(token);
        req.accountId = decoded.id;
                
        next();
    } catch(error) {
        return res.jsonUnauthorized(null, "Invalid token");
    }

}

module.exports = checkJwt;