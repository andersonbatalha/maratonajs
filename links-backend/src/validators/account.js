const Joi = require("@hapi/joi");
const { getValidatorError } = require('../helpers/validator');

const rules = {
    email: Joi.string().required().email(),
    password: Joi.string().required().regex(new RegExp('^[a-zA-Z0-9]{3,30}$')).min(5).max(50),
    password_confirmation: Joi.string().required().valid(Joi.ref('password')),
};

const options = { abortEarly: false };

const accountSignIn = (req, res, next) => {
    const { email, password } = req.body;

    const schema = Joi.object({
        email: rules.email,
        password: rules.password,
    });

    const { error } = schema.validate( { email, password }, options );

    if (error) {
        const messages = getValidatorError(error, 'account.signin');
        return res.jsonBadRequest(null, null, { error: messages });
    };

    next();
}

const accountSignUp = (req, res, next) => {
    const { email, password, password_confirmation } = req.body;

    const schema = Joi.object({
        email: rules.email,
        password: rules.password,
        password_confirmation: rules.password_confirmation,
    });

    const { error } = schema.validate( {email, password, password_confirmation}, options );

    if (error) {
        const messages = getValidatorError(error, 'account.signup');
        return res.jsonBadRequest(null, null, { error: messages });
    };

    next();
}

module.exports = { accountSignUp, accountSignIn };