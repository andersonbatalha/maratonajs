require('dotenv').config();

const jwt = require('jsonwebtoken');
const tokenPrivateKey = process.env.JSON_WEB_TOKEN;
const tokenRefreshPrivateKey = process.env.JSON_REFRESH_WEB_TOKEN;
const options = { expiresIn: '30 minutes'};
const options_refresh = { expiresIn: '30 days'};

const generateJwt = (payload) => {
    return jwt.sign(payload, tokenPrivateKey, options);
};

const generateRefreshJwt = (payload) => {
    return jwt.sign(payload, tokenRefreshPrivateKey, options_refresh);
};

const verifyJwt = (token) => {
    return jwt.verify(token, tokenPrivateKey);
};

const verifyRefreshJwt = (token) => {
    return jwt.verify(token, tokenRefreshPrivateKey);
};

const getTokenFromHeaders = (headers) => {
    let token = headers['authorization'];
    return token ? token.slice(7, token.length) : null;
}

module.exports = { generateJwt, generateRefreshJwt, verifyJwt, verifyRefreshJwt, getTokenFromHeaders };