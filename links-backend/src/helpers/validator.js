const { getMessages } = require('./messages'); 

const getValidatorError = (error, messagePath) => {
    if (!error) { return null; }

    const errorMessages = {};

    error.details.map(detail => {
        const errorKey = detail.context.key;
        const errorMessage = detail.message;
        const errorType = detail.type;

        const errorPath = `${messagePath}.${errorKey}.${errorType}`

        console.log(`\"${errorPath}\": \"${errorMessage}\"` );
        errorMessages[errorKey] = getMessages(errorPath);        
    });

    return errorMessages;
}

module.exports = { getValidatorError, getMessages };