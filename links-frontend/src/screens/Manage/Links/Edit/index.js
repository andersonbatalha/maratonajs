import React, { useEffect } from 'react';
import { useParams } from "react-router-dom";
import { linkGet, linkUpdate, linkClear } from '../../../../actions/LinkActions';
import { connect } from 'react-redux';

import Layout from '../../../Layout/Manage';
import FormGroup from '../../../../components/FormGroup';
import FormCheck from '../../../../components/FormChecked';
import { getFormData } from '../../../../helpers/form';

const Edit = ({ link, linkGet, linkUpdate, linkClear }) => {
    const { id } = useParams();

    useEffect( () => {
        linkGet(id);
        return () => { 
            linkClear();
        };
    }, [id, linkGet, linkClear]);

    const submitHandler = (e) => {
        e.preventDefault();
        const data = getFormData(e);
        linkUpdate(id, data);
    };
    
    return (
        <Layout>
            <div className="container">
                <h1>Edit Link</h1>
                <form onSubmit={submitHandler}>
                    <FormGroup type="text" name="label" label="Label" data={link} />
                    <FormGroup type="text" name="url" label="URL" data={link} />
                    <FormCheck name="isSocial" label="Is Social?" data={link} />   
                    <div className="container text-center fixed-bottom pb-5">
                        <button type="submit" className="btn btn-primary btn-round btn-lg">Submit</button>
                    </div>
                </form>
            </div>
        </Layout>
    );
};

const mapStateToProps = (state) => {
    return {
        link: state.link.link,
    };
}

export default connect(mapStateToProps, { linkGet, linkClear, linkUpdate })(Edit);    