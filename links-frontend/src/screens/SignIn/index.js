import React from 'react';
import { Link, Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { getFormData } from '../../helpers/form';

import { signIn } from '../../actions/AccountActions';

const SignIn = (props) => {
    const { signIn, account } = props;

    if (account) return <Redirect to="/manage/links" />;

    const submitHandler = (e) => {
        e.preventDefault();
        const data = getFormData(e);
        signIn(data);
    };

    return (
        <div className="container">
            <h1>Sign In</h1>
            <div className="d-flex flex-column h-100">
                <form onSubmit={ submitHandler }>
                    <div className="form-group">
                        <label>E-mail</label>
                        <input type="text" className="form-control" name="email"></input>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" name="password"></input>
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary btn-round">Submit</button>
                    </div>
                </form>
                <div className="container text-center fixed-bottom pb-5">
                    <div className="text-muted">
                        <p>Don't have an account?</p>
                        <Link to='/sign-up'>Sign up</Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        account: state.account.account,
    };
}

export default connect(mapStateToProps, { signIn })(SignIn);    