import React, { useEffect } from 'react';
import { Route, Switch, BrowserRouter } from "react-router-dom";
import { connect } from 'react-redux';

import { initAccount } from './actions/AccountActions';

import Home from "./screens/Home";
import SignIn from "./screens/SignIn";
import SignUp from "./screens/SignUp";
import Edit from "./screens/Manage/Links/Edit";
import Create from "./screens/Manage/Links/Create";
import Links from "./screens/Manage/Links";

import './styles/main.scss';

const App = ({ initAccount }) => {

    useEffect(() => {
        initAccount()
    }, [initAccount]);

    return (
      <BrowserRouter>
        <div>
            <Switch>
                <Route path="/auth/sign-in">
                    <SignIn />
                </Route>
                <Route path="/auth/sign-up">
                    <SignUp />
                </Route>
                <Route path="/manage/links/edit/:id">
                    <Edit />
                </Route>
                <Route path="/manage/links/create">
                    <Create />
                </Route>
                <Route path="/manage/links">
                    <Links />
                </Route>
                <Route path="/">
                    <Home />
                </Route>
            </Switch>
        </div>
      </BrowserRouter>
    );
};

const mapStateToProps = (state) => {
    return {
        account: state.account.account,
    };
}

export default connect(mapStateToProps, { initAccount })(App);    