import React, { useEffect, useState } from 'react';

const FormGroup = (props) => {
    const { type, data, label, name } = props;
    const [ value, setValue ] = useState('');

    useEffect( () => {
        const initialValue = data && data[name] ? data[name] : undefined;
        if (initialValue !== undefined) setValue(initialValue);
    }, [name, data]);

    const handleChange = (e) => {
        if (value === e.target.value) return;
        setValue(e.target.value);
    }

    const inputProps = {
        type,
        label,
        value: value || '',
        name,
        onChange: handleChange,
    };

    return (
        <div className="form-group">
            <label>{label}</label>
            <input type="text" className="form-control" {...inputProps} />
        </div>
    );
};

export default FormGroup;