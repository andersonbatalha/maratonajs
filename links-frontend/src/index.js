import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import TokenRefresher from './components/TokenRefresher';

import { Provider } from 'react-redux';

import store from './store';

ReactDOM.render(
    <Provider store={store}>
        <TokenRefresher />
        <App />
    </Provider>, document.getElementById('root'));